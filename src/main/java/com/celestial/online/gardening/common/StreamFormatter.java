/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.online.gardening.common;

import com.celestial.online.gardening.bos.Account;
import com.celestial.online.gardening.bos.Customer;

/**
 *
 * @author Selvyn
 */
public interface StreamFormatter
{    
    public  String  format( Account acc );

    public  String  format( Customer acc );
}
