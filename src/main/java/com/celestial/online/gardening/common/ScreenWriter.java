/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.online.gardening.common;

/**
 *
 * @author Selvyn
 */
public class ScreenWriter   implements  OutputChannel
{
    @Override
    public  void    write( Object dataStream )
    {
        System.out.printf("%s", dataStream );
    } 
    
    @Override
    public  void    close()
    {
    }

    @Override
    public void openForWriting(String fname)
    {
        throw new UnsupportedOperationException("Not supported in this class."); //To change body of generated methods, choose Tools | Templates.
    }

}
