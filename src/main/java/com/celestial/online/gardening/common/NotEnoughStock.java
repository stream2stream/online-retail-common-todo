/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.online.gardening.common;

/**
 *
 * @author Selvyn
 */
public class NotEnoughStock extends Exception
{
    public  NotEnoughStock( String msg )
    {
        super( msg );
    }
}
