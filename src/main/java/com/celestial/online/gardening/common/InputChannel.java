/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.online.gardening.common;

/**
 *
 * @author Selvyn
 */
public interface InputChannel
{
    public  void    openForReading( String fname );
    public  Object  read();
    public  void    close();    
}
