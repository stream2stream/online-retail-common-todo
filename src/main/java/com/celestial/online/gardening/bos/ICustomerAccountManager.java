/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.online.gardening.bos;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Selvyn
 */
public interface ICustomerAccountManager    extends Serializable
{
    public  int nextAccountNo();
    
    public String genkey();

    public HashMap<String, Customer> getCustomers();

    public HashMap<Integer, Account> getAccounts();

    public Customer getCustomer(String cid);

    public Account getAccount(int accId);

    public void addCustomer(String cid, Customer cc);

    public void addAccount(Account acc);
}
