/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.online.gardening.bos;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author Selvyn
 */
public class CustomerAccountMgrFactory implements Serializable
{

    private static ICustomerAccountManager itsCustomerAccountMgr;

    public ICustomerAccountManager getCustomerAccountMgr()
    {
        if (itsCustomerAccountMgr == null)
        {
            itsCustomerAccountMgr = new CustomerAccountMgr();
        }

        return itsCustomerAccountMgr;
    }

    public void setCustomerAccountMgr(ICustomerAccountManager caMgr)
    {
        itsCustomerAccountMgr = caMgr;
    }

    public class CustomerAccountMgr implements ICustomerAccountManager
    {

        private final HashMap< String, Customer> customers = new HashMap<>();
        private final HashMap< Integer, Account> accounts = new HashMap<>();
        private int currentAccNo = 10;

        private CustomerAccountMgr()
        {
        }

        @Override
        public int nextAccountNo()
        {
            return currentAccNo++;
        }
        
        @Override
        public String genkey()
        {
            long currentTimeInMilliSecs = new Date().toInstant().toEpochMilli();
            int secs = (int) (currentTimeInMilliSecs / 1000) % 60;
            String result = "";
            Random gen = new Random();

            for (int x = 0; x < 3; x++)
            {
                result += (char) (gen.nextInt(26) + 'A');
            }
            result += secs;

            return result;
        }

        @Override
        public HashMap<String, Customer> getCustomers()
        {
            return this.customers;
        }

        @Override
        public HashMap<Integer, Account> getAccounts()
        {
            return this.accounts;
        }

        @Override
        public Customer getCustomer(String cid)
        {
            return customers.get(cid);
        }

        @Override
        public Account getAccount(int accId)
        {
            return accounts.get(accId);
        }

        @Override
        public void addCustomer(String cid, Customer cc)
        {
            customers.put(cid, cc);
        }

        @Override
        public void addAccount(Account acc)
        {
            accounts.put(acc.getAccountId(), acc);
        }
    }
}
